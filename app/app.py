from flask import Flask, jsonify
from flask_restful import Api
from werkzeug.exceptions import HTTPException
from werkzeug.exceptions import default_exceptions
from eflint.instance_manager import InstanceManager
import settings
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
from flask_migrate import Migrate
 
app = Flask(__name__)

eflint_instance_manager = InstanceManager()

@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(error=str(e)), code

for ex in default_exceptions:
    app.register_error_handler(ex, handle_error)

def handle_signal(signum, frame):
    print('Exitting...')
    
    res = readchar.readchar()
    if res == 'y':
        print("")
        exit(1)
    else:
        print("", end="\r", flush=True)
        print(" " * len(msg), end="", flush=True) # clear the printed line
        print("    ", end="\r", flush=True)


app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
app.config['BUNDLE_ERRORS'] = settings.BUNDLE_ERRORS

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)
# api.prefix = '/api'

INTERVAL_TASK_ID = 'ticking-clock'
scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()

from routes.instance import InstanceResource

api.add_resource(InstanceResource, '/instances')
scheduler.add_job(id=INTERVAL_TASK_ID, func=InstanceResource.tick, trigger='interval', seconds=10)

if __name__ == '__main__':
        app.run(host="0.0.0.0")
