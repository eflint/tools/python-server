

class ReasonerDoesNotExistException(Exception):
    "Thrown when the given uuid is not known"
    pass