from app import db

class Instance(db.Model):
    __tablename__ = 'instances'

    uuid = db.Column(db.String(64), primary_key=True)
    
    model_location = db.Column(db.String(255))
    state = db.Column(db.Text())

    port = db.Column(db.Integer())

    clock = db.Column(db.Boolean())

    def __repr__(self):
        return 'eFLINT instance with uuid: {}'.format(self.uuid)