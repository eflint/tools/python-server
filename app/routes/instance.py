from flask_restful import Resource, reqparse, request
from flask import request as flaskRequest
from flask_restful import fields, marshal_with, marshal
from models.instance import Instance
from app import db
from app import eflint_instance_manager
from app import app
from exceptions.reasoner_does_not_exist_exception import ReasonerDoesNotExistException
from app import scheduler

instance_fields = {
    'uuid': fields.String,
    'model_location': fields.String,
}

instance_list_fields = {
    'count': fields.Integer,
    'instances': fields.List(fields.Nested(instance_fields)),
}

instance_post_parser = reqparse.RequestParser()
instance_post_parser.add_argument('model_location', type=str, required=True, location=['json'],
                              help='model location parameter is required')
instance_post_parser.add_argument('clock', type=bool, required=False, location=['json'])

class InstanceResource(Resource):
    def get(self, instance_id=None):
        if instance_id:
            instance = Instance.query.filter_by(uuid=instance_id).first()
            return marshal(instance, instance_fields)
        else:
            args = request.args.to_dict()
            limit = args.get('limit', 0)
            offset = args.get('offset', 0)

            args.pop('limit', None)
            args.pop('offset', None)

            instance = Instance.query.filter_by(**args).order_by(Instance.uuid)
            if limit:
                instance = instance.limit(limit)

            if offset:
                instance = instance.offset(offset)

            instance = instance.all()

            return marshal({
                'count': len(instance),
                'instances': [marshal(instance_model, instance_fields) for instance_model in instance]
            }, instance_list_fields)

    @marshal_with(instance_fields)
    def post(self):
        args = instance_post_parser.parse_args()

        if args['clock']:
            instance = create_and_store_reasoner_instance(args['model_location'], clock=args['clock'])
        else:
            instance = create_and_store_reasoner_instance(args['model_location'])

        return instance

    @marshal_with(instance_fields)
    def delete(self, instance_uuid=None):
        

        instance = Instance.query.get(instance_uuid)

        eflint_instance_manager.kill_instance(instance_uuid)

        db.session.delete(instance)
        db.session.commit()

        return None

    @app.route('/instances/command', methods=['GET'])
    def command():
        uuid = flaskRequest.args['uuid']
        command = flaskRequest.args['command']

        instance = Instance.query.filter_by(uuid=uuid).first()

        if instance == None:
            raise ReasonerDoesNotExistException

        if not eflint_instance_manager.is_reasoner_running(instance.uuid):
            recreate_instance(instance)
        
        response = eflint_instance_manager.send_command(uuid, command)

        export_command = '{"command": "create-export"}'

        export = eflint_instance_manager.send_command(uuid, export_command)

        instance_to_update = Instance.query.get(uuid)
        instance_to_update.state = export.strip()

        db.session.commit()
        
        return str(response)

    def tick():
        with scheduler.app.app_context():
            for instance in Instance.query.filter_by(clock=True).all():
                if eflint_instance_manager.is_reasoner_running(instance.uuid):
                    tick_command = '{"command": "phrase", "text":"tick()"}'
                    eflint_instance_manager.send_command(instance.uuid, tick_command)
                else:
                    recreate_instance(instance)

def create_and_store_reasoner_instance(model_location, uuid=None, clock=False):
    args = {}

    eflint_server_instance = eflint_instance_manager.create_instance(model_location, uuid)
    args['uuid'] = eflint_server_instance.uuid
    args['port'] = eflint_server_instance.port
    args['model_location'] = eflint_server_instance.model_location
    args['clock'] = clock

    instance = Instance(**args)
    db.session.add(instance)
    db.session.commit()

    return eflint_server_instance

def recreate_instance(instance):
    eflint_instance = eflint_instance_manager.create_instance(instance.model_location, instance.uuid)

    # if instance.state:
    #     import_command = '{"command":"load-export", "graph":' + instance.state + '}'
    #     print(import_command)
    #     response = eflint_instance_manager.send_command(instance.uuid, import_command)
    #     print(response)
    return True

    