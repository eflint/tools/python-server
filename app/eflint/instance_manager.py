from subprocess import Popen, PIPE, STDOUT
from eflint.eflint_server_instance import EFLINTServerInstance
import random
import socket
import uuid
import time
# from models.instance import Instance

class InstanceManager():
    instance_map = {}

    def create_instance(self, model_location, instance_uuid = None):
        if instance_uuid == None:
            instance_uuid = str(uuid.uuid4())
        
        port = random.randint(1025, 65536)

        eflint_server_process = self.start_process(model_location, port)
        
        print(eflint_server_process)
        eflint_server_instance = EFLINTServerInstance(instance_uuid, port, eflint_server_process, model_location)
        self.instance_map[instance_uuid] = eflint_server_instance

        return eflint_server_instance
        
    def kill_instance(self, uuid):
        self.instance_map[uuid].process.kill()
        self.instance_map.pop(uuid)

    def kill_all_instances(self):
        for uuid in self.instance_map.keys:
            self.kill_instance(uuid)

    def send_command(self, uuid, command):
        eflint_server_instance = self.instance_map[uuid]

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(('localhost', eflint_server_instance.port))
            s.sendall((command + '\n').encode('utf-8'))
            response = ''

            while True:
                chunk = s.recv(4096)
                chunk = chunk.decode('utf-8', 'ignore')
                response += str(chunk)
                if '\n' in response:
                    break
        return response

    def is_reasoner_running(self, uuid):
        return uuid in self.instance_map.keys() and self.is_reasoner_alive(uuid)

    def get_reasoner_state(self, uuid):
        response = self.send_command(uuid, "export")

    def start_process(self, model_location, port):
        eflint_server_process = Popen(['eflint-server', model_location, str(port)], stdout=PIPE, stderr=PIPE)
        time.sleep(3)
        return eflint_server_process

    def is_reasoner_alive(self, uuid):
        poll = self.instance_map[uuid].process.poll()
        if poll is None:
            return True
        return False