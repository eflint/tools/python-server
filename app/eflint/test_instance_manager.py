from subprocess import Popen, PIPE
import random
import uuid
import time

if __name__ == '__main__':
    instance_map = {}

    port = random.randint(0, 65536)
    model_location = '/Users/marten/Documents/uva/assistentschap/eflint/eflint-examples-main/individuele-inkomenstoeslag/regels.eflint'

    eflint_server_process = Popen(['eflint-server', model_location, str(port)], stdout=PIPE, stderr=PIPE)
    instance_uuid = uuid.uuid4()

    instance_map[instance_uuid] = eflint_server_process

    print(instance_map[instance_uuid].pid)

    instance_map[instance_uuid].kill()

    time.sleep(2)

    poll = instance_map[instance_uuid].poll()
    if poll is not None:
        print(str(instance_map[instance_uuid].pid) + ' is not alive')

