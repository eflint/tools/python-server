FROM haskell:9.2.7 as eflint

# Prepare the directory to build in
RUN mkdir /tmp/eflint-project
WORKDIR /tmp/eflint-project

# Prepare cabal
RUN cabal update 

# Install eFLINT
COPY haskell-implementation .
RUN cabal install

FROM tiangolo/uwsgi-nginx:python3.11
COPY ./app /eflint-instance-manager
COPY --from=eflint /root/.cabal/bin/eflint-server /usr/bin/eflint-server

WORKDIR /eflint-instance-manager

RUN ["pip", "install", "--no-cache-dir", "--upgrade", "-r", "./requirements.txt"]
RUN ["flask", "db", "init"]
RUN ["flask", "db", "migrate"]
RUN ["flask", "db", "upgrade"]

CMD ["flask", "run", "-h", "0.0.0.0"]
EXPOSE 5000
