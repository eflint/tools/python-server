# eFLINT instance manager
This application canc be used to manage eFLINT instances and communicate with these instances over HTTP. Using the import and export functionality of the eFLINT haskell-implementation, the instance manager tries to guarantee a consistent state even between reboots.

# Running the instance manager

## Install dependencies
The eFLINT instance manager depends on Flask to handle request and serve the user a response. To install all required packages follow these steps:
    - Make sure you have python and python virtualenv installed
    - Create a virtual environment using python3 -m venv .
    - Activate the python virtual environment using . .venv/bin/activate
    - Install all dependencies using pip3 install -r requirements.txt

## Setup the database
After installing all required dependencies the database needs to be created and migrated to make sure the eFLINT state is stored correctly. Use the following commands to migrate the database:
    - flask db init
    - flask db migrate
    - flask db upgrade

## Run the instance manager
With a successfully migrated database you can start the eFLINT instance manager in development mode by running:
    - flask run

## HTTP 

### Create Instance

Create a new eFLINT server instance using the given model location as a base eFLINT file.

**URL** : `/instances`

**Method** : `POST`

**Data examples**

To create an instance a model_location needs to be given in the json body of the request.

```json
{
    "model_location": "/path/to/eflint/file.eflint"
}
```

Optionally you can add the clock parameter to the body to enable the time management of the instance manager. If the clock is enabled it periodically (every 10 seconds by default) sends the phrase ``` tick() ``` to the reasoner.

```json
{
    "model_location": "/path/to/eflint/file.eflint",
    "clock": true
}
```

#### Success Response

**Code** : `200 OK`

**Content examples**

For a User with ID 1234 on the local database where that User has saved an
email address and name information.

```json
{
	"uuid": "ff7002de-3991-4669-a2ec-ec9c0d217fd4",
	"model_location": "/path/to/eflint/file.eflint"
}
```

### Run command

Run a command on the eFLINT reasoner. For all available commands see: https://gitlab.com/eflint/haskell-implementation/-/blob/master/README.md
**URL** : `/instances/command`

**Parameters** :
    - uuid: The uuid of the reasoner to run the command on.
    - command: The JSON of the command to be ran.

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

This endpoint returns the response from the eFLINT reasoner. For a full list see https://gitlab.com/eflint/haskell-implementation/-/blob/master/README.md.


### Get reasoners

Get the list of active reasoners and their uuid.

**URL** : `/instances`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

The list of active eFLINT reasoners.

{
	"count": 1,
	"instances": [
		{
			"uuid": "b46c57d4-75f8-4e49-98e8-52b6c2f7a5da",
			"model_location": "/path/to/eflint/file.eflint"
		},
	]
}